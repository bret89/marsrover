*****************************************
* [Welcome to MarRover brand new game!] *
*****************************************

1 Installation:

    - First of all make git clone from this repository in your apache root directory in your server.

      ">git clone https://bret89@bitbucket.org/bret89/marsrover.git"

    - Follow symfony's installation guide (https://symfony.com/doc/current/setup.html) to be ensured 
      you have installed required software (PHP >= 5.4, composer).

    - This aplication requires MySQL server:
        - Default configuration is as follows (you can change it in: app/config/parameters.yml):
      
            database_host: 127.0.0.1
            database_port: 3306
            database_name: mars_rover
            database_user: root
            database_password: null

        - To create the database execute this symfony comand in the console:
            
            ">php bin/console doctrine:database:create"  
            ">php bin/console doctrine:schema:create"

          If you prefers to do it manually, you can tip "--dump-sql" at the end of the above command and apply
          its SQL sentences directly in the MySQL console(MySQLWorbenk, PhMyAdmin on terminal).

        * Once the database and its schema are created correctly, please, insert this required 
          data in mars_rover directions table. 

        INSERT INTO `mars_rover`.`directions` (`direction`) VALUES ('North');
        INSERT INTO `mars_rover`.`directions` (`direction`) VALUES ('East');
        INSERT INTO `mars_rover`.`directions` (`direction`) VALUES ('South');
        INSERT INTO `mars_rover`.`directions` (`direction`) VALUES ('West');


        - Update composer dependences. ">composer install"

        - Execute symfony clear cache command in symfony's console: ">php bin/console cache:clear --env=prod --no-debug"

* All relative routes shown in this file are from project's root directory (path/to/apache/MarsRover).

2 Execute aplication:

    - Once you do all above steps, you should be able to start a new game requesting the follow URL:
        
        http://localhost/MarsRover/web/app.php

3 Error resolutions:

    - Please contact with repository owner (Jaume Bret Solé) by mail bret89@gmail.com.

Thank you!