<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Game;
use AppBundle\Entity\Directions;
use AppBundle\GameServer\Movement;

use AppBundle\Form\GameType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="start_game")
     */
    public function startGameAction(Request $request)
    {        
        $em = $this->getDoctrine()->getManager();
        
        if($request->hasPreviousSession()){
            $gameId = $request->getSession()->get("game_id");
            $request->getSession()->remove("game_id");
            if(is_numeric($gameId)){
                $game = $this->getDoctrine()->getRepository(Game::class)->find($gameId);     
                if($game){
                    $em->remove($game);
                    $em->flush();                                
                }
            }
        } 
        
        $game = new Game();
                
        $form = $this->createForm(GameType::class, $game, array("action" => $this->generateUrl("start_game"), 
                                                                "method" => "POST", 
                                                                "entity_manager" => $this->get("doctrine.orm.entity_manager")));
        $form->add('Next step', SubmitType::class);
        
        $form->handleRequest($request);
                
        if($form->isSubmitted() && $form->isValid()){
            
                        
            $currentDirection = $this->getDoctrine()->getRepository(Directions::class)->find($game->getCurrentDirection());
            
            if(!$currentDirection){
                throw new Exception("The selected direction do not exist!", 1, null);
            }
            
            $game->setCurrentDirection($currentDirection);
            
            $em->persist($game);             
            $em->flush();
                        
            /**
             * Init user session with game id
             */
            $request->getSession()->set("game_id", $game->getId());
            
            return $this->redirectToRoute("set_game_obstacles");
        }
        
        return $this->render("::game/index.html.twig", array("form" => $form->createView()));
    }
    
    /**
     * @Route("/set-game-obstacles", name="set_game_obstacles")
     */
    public function setGameObstacles(Request $request)
    {
        if($request->hasPreviousSession()){
            $gameId = $request->getSession()->get("game_id");
            if(is_numeric($gameId)){
                $game = $this->getDoctrine()->getRepository(Game::class)->find($gameId);                             
            }
        } 
        
        if(!$game){
            return $this->redirectToRoute("start_game");
        }
        
        /**
         * If game is set
         */
        $obstacle = new \AppBundle\Entity\GameObstacles();
        $obstacle->setGame($game);
        
        $form = $this->createForm(\AppBundle\Form\GameObstaclesType::class, $obstacle, array("action" => $this->generateUrl("set_game_obstacles"),
                                                                                     "method" => "post"));
        
        $form->add("Add new obstacle", SubmitType::class);
        $form->add("Start Mars expedition", SubmitType::class);
        
        $form->handleRequest($request);      
        
        if($form->isValid() && $form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            /* @var $game Game */
            $em->persist($obstacle);
            $em->flush();            
            
            if(preg_match("/start/i", $form->getClickedButton()->getName())){
                return $this->redirectToRoute("play");
            }
            return $this->redirectToRoute("set_game_obstacles");            
        }
        
        return $this->render("::game/set_obstacles.html.twig", array("form" => $form->createView(), "game" => $game));        
    }
    
    /**
     * @Route("/play", name="play")
     * @param Request $request
     */
    public function playAction(Request $request)
    {
        if($request->hasPreviousSession()){
            $gameId = $request->getSession()->get("game_id");
            if(is_numeric($gameId)){
                $game = $this->getDoctrine()->getRepository(Game::class)->find($gameId);
                if($game){
                    return $this->render("::game/play.html.twig", array("game" => $game));
                }                                
            }
        }                        
                                
        return $this->redirectToRoute("start_game");
    }
    
    /**
     * @Route("/move", name="move")
     * @param Request $request
     */
    public function movementAction(Request $request)
    {
        if($request->hasPreviousSession()){
            $gameId = $request->getSession()->get("game_id");
            if(is_numeric($gameId)){
                $game = $this->getDoctrine()->getRepository(Game::class)->find($gameId);                             
            }
        } 
        
        /* @var $game Game */
                
        $pressedKey = $request->get("pressedKey");
                
        if(!$game || !$pressedKey){
            return $this->json(array("result" => false));
        }
        
        $directions = $this->getDoctrine()->getRepository(Directions::class)->findAll();
        
        $gameMovementService = new Movement($game, $pressedKey, $directions);
                        
        try{
            $gameMovementService->doMovement();
            $game->setCurrentRow($gameMovementService->getNewCoordinate()->getRow());
            $game->setCurrentCol($gameMovementService->getNewCoordinate()->getCol());
            $game->setCurrentDirection($gameMovementService->getNewCoordinate()->getDirection());
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($game);
            $em->flush();
        }
        catch(\Exception $e){
            return $this->json(array("result" => false, "message" => $e->getMessage()));
        }
        
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);        
        
        return $this->json(array("result" => true, "coordinate" => $serializer->normalize($gameMovementService->getNewCoordinate())));
    }
}
