<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use \Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GameType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $em = $options['entity_manager'];
        /* @var $em \Doctrine\ORM\EntityManager */
        $directions = $em->getRepository(\AppBundle\Entity\Directions::class)->findAll();
        
        $directionChoices = array();
        foreach($directions as $direction){
            $directionChoices[$direction->getDirection()] = $direction->getId();
        }
                
        asort($directionChoices);
                
        $builder
            ->add('rows', IntegerType::class, array("label" => "Rows", "attr" => array("min" => 1, "ng-model" => "rows")))
            ->add('cols', IntegerType::class, array("label" => "Cols", "attr" => array("min" => 1, "ng-model" => "cols")))
            ->add('currentRow', IntegerType::class, array("label" => "Start row", "attr" => array("min" => 1, "max" => "{{ rows }}")))
            ->add('currentCol', IntegerType::class, array("label" => "Start col", "attr" => array("min" => 1, "max" => "{{ cols}}")))
            ->add('currentDirection', ChoiceType::class, array("label" => "Start direction", "choices" => $directionChoices))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Game'
        ))->setRequired('entity_manager');
        
    }
}
