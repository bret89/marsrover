<?php

namespace AppBundle\GameServer;

use AppBundle\Entity\Game;
use AppBundle\Entity\Coordinate;

use Symfony\Component\Debug\Debug;

Debug::enable();

class Movement extends GameService {
    
    protected $pressedKey;
    
    /**
     *
     * @var Coordinate
     */
    protected $newCoordinate;
    
    protected $directions;


    public function __construct(Game $game, $pressedKey, $directions) {
        parent::__construct($game);
        $this->pressedKey = $pressedKey;  
        $this->directions = $directions;
    }
    
    private function getPressedKey() {
        return $this->pressedKey;
    }
    
    /**
     * 
     * @return Coordinate
     */
    public function getNewCoordinate() {
        return $this->newCoordinate;
    }
    
    private function getDirections() {
        return $this->directions;
    }
            
    protected function setNewCoordinate(Coordinate $newCoordinate) {
        $this->newCoordinate = $newCoordinate;
    }              
                    
    /**
     * This method returns JSON with the new mars rover coordinate and direction
     * @throws Exception
     */
    public function doMovement()
    {     
        $newCoordinate = $this->calculateNewCoordinate();
        if($this->isValidCoordinate($newCoordinate)){
            $this->setNewCoordinate($newCoordinate);
            return true;
        }  
        else{
            throw new \Exception("There is an obstacle in this way!");
        }
    }   
    
    private function calculateNewCoordinate()
    {      
        /**
         * If pressedKey change Mars Rover direction
         */
        if($this->isDirectionChange()){
            return new Coordinate($this->getGame()->getCurrentRow(), $this->getGame()->getCurrentCol(), $this->getNewDirection());
        }
        /**
         * If pressedKey change Mars Rover position
         */
        else{
            if($this->isMovingForward()){
                if($this->isMarsRoverPointingNorthOrSouth()){
                    if($this->isMarsRoverPointingNorth()){
                        return new Coordinate($this->mod($this->getGame()->getCurrentRow()-2, $this->getGame()->getRows())+1, $this->getGame()->getCurrentCol(), $this->getGame()->getCurrentDirection());;
                    }
                    else{
                        return new Coordinate($this->mod($this->getGame()->getCurrentRow(), $this->getGame()->getRows())+1, $this->getGame()->getCurrentCol(), $this->getGame()->getCurrentDirection());
                    }
                }
                else{
                    if($this->isMarsRoverPointingEast()){
                        return new Coordinate($this->getGame()->getCurrentRow(), $this->mod($this->getGame()->getCurrentCol(), $this->getGame()->getCols())+1, $this->getGame()->getCurrentDirection());
                    }
                    else{
                        return new Coordinate($this->getGame()->getCurrentRow(), $this->mod($this->getGame()->getCurrentCol()-2, $this->getGame()->getCols())+1, $this->getGame()->getCurrentDirection());
                    }
                }
            }
            else{
                if($this->isMarsRoverPointingNorthOrSouth()){
                    if($this->isMarsRoverPointingNorth()){
                        return new Coordinate($this->mod($this->getGame()->getCurrentRow(), $this->getGame()->getRows())+1, $this->getGame()->getCurrentCol(), $this->getGame()->getCurrentDirection());
                    }
                    else{
                        return new Coordinate($this->mod($this->getGame()->getCurrentRow()-2, $this->getGame()->getRows())+1, $this->getGame()->getCurrentCol(), $this->getGame()->getCurrentDirection());
                    }
                }
                else{
                    if($this->isMarsRoverPointingEast()){
                        return new Coordinate($this->getGame()->getCurrentRow(), $this->mod($this->getGame()->getCurrentCol()-2, $this->getGame()->getCols())+1, $this->getGame()->getCurrentDirection());
                    }
                    else{
                        return new Coordinate($this->getGame()->getCurrentRow(), $this->mod($this->getGame()->getCurrentCol(), $this->getGame()->getCols())+1, $this->getGame()->getCurrentDirection());
                    }
                }
            }            
        }
    }
       
    private function isValidCoordinate(Coordinate $coordinate)
    {
        foreach($this->getGame()->getObstacles() as $obstacle){
            if($obstacle->equalsTo($coordinate)){
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Returns is change direction order
     * @return boolean
     */
    private function isDirectionChange()
    {
        if($this->isClockwiseDirectionChange() || $this->isCounterclockwiseDirectionChange()){
            return true;
        }
        
        return false;
    }
    
    private function isClockwiseDirectionChange()
    {
        if(preg_match("/arrowright/i", $this->getPressedKey())){
            return true;
        }
        
        return false;
    }
    
    private function isCounterclockwiseDirectionChange()
    {
        if(preg_match("/arrowleft/i", $this->getPressedKey())){
            return true;
        }
        
        return false;
    }   
    
    private function getNewDirection()
    {        
        $directionsChord = array();
        
        foreach($this->getDirections() as $direction){
            /* @var $direction \AppBundle\Entity\Directions */
            $directionsChord[$direction->getId()] = $direction;
        }
                
        $currentDirection = $this->getGame()->getCurrentDirection()->getId();
        
        if($this->isClockwiseDirectionChange()){
            $newDirection = $this->mod($currentDirection, count($this->getDirections()));
        }
        else{
            $newDirection = $this->mod($currentDirection-2, count($this->getDirections()));
        }
                
        return $directionsChord[$newDirection+1];
    }
    
    /**
     * Return direction entity object fetching one id
     * @param type $id
     * @return type
     */
    private function getDirectionById($id)
    {
        foreach($this->getDirections() as $direction){
            /* @var $direction \AppBundle\Entity\Directions */
            if($direction->getId() == $id){
                return $direction;
            }
        }
        
        return null;
    }
    
    private function isMovingForward()
    {
        if(preg_match("/arrowup/i", $this->getPressedKey())){
            return true;
        }
        
        return false;
    }
    
    private function isMarsRoverPointingNorthOrSouth()
    {
        if($this->isMarsRoverPointingNorth() || $this->isMarsRoverPointingSouth()){
            return true;
        }
        
        return false;
    }
    
    private function isMarsRoverPointingNorth()
    {
        if(in_array($this->getGame()->getCurrentDirection()->getId(), array(1))){
            return true;
        }
        
        return false;
    }
    
    private function isMarsRoverPointingSouth()
    {
        if(in_array($this->getGame()->getCurrentDirection()->getId(), array(3))){
            return true;
        }
        
        return false;
    }
    
    private function isMarsRoverPointingEastOrWest()
    {
        if($this->isMarsRoverPointingEast() || $this->isMarsRoverPointingWest()){
            return true;
        }
        
        return false;
    }
    
    private function isMarsRoverPointingEast()
    {
        if(in_array($this->getGame()->getCurrentDirection()->getId(), array(2))){
            return true;
        }
        
        return false;
    }
    
    private function isMarsRoverPointingWest()
    {
        if(in_array($this->getGame()->getCurrentDirection()->getId(), array(4))){
            return true;
        }
        
        return false;
    }
    
    /**
     * Return the arithmetic module between given numbers
     * @param type $a
     * @param type $b
     * @return type
     */
    private function mod($a, $b)
    {        
        return $a-(floor($a / $b) * $b);        
    }
}
