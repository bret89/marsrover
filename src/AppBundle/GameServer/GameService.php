<?php

namespace AppBundle\GameServer;

use AppBundle\Entity\Game;

class GameService {
    
    protected $game;

    public function __construct(Game $game) {
        $this->game = $game;
    }
    
    /**
     * 
     * @return \AppBundle\Entity\Game
     */
    protected function getGame() {
        return $this->game;
    }   
}
