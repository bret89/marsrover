<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="rows", type="integer")
     */
    private $rows;

    /**
     * @var int
     *
     * @ORM\Column(name="cols", type="integer")
     */
    private $cols;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime")
     */
    private $startTime;

    /**
     * @var int
     *
     * @ORM\Column(name="current_row", type="integer")
     */
    private $currentRow;

    /**
     * @var int
     *
     * @ORM\Column(name="current_col", type="integer")
     */
    private $currentCol;

    /**
     * @ORM\ManyToOne(targetEntity="Directions")
     * @ORM\JoinColumn(name="current_direction", referencedColumnName="id")
     */
    private $currentDirection;
    
    /**    
     * @ORM\OneToMany(targetEntity="GameObstacles", mappedBy="game", cascade="remove")
     */
    private $obstacles;
    
    public function __construct() {
        $this->obstacles = new ArrayCollection();
        $this->startTime = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rows
     *
     * @param integer $rows
     *
     * @return Game
     */
    public function setRows($rows)
    {
        $this->rows = $rows;

        return $this;
    }

    /**
     * Get rows
     *
     * @return int
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * Set cols
     *
     * @param integer $cols
     *
     * @return Game
     */
    public function setCols($cols)
    {
        $this->cols = $cols;

        return $this;
    }

    /**
     * Get cols
     *
     * @return int
     */
    public function getCols()
    {
        return $this->cols;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Game
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set currentRow
     *
     * @param integer $currentRow
     *
     * @return Game
     */
    public function setCurrentRow($currentRow)
    {
        $this->currentRow = $currentRow;

        return $this;
    }

    /**
     * Get currentRow
     *
     * @return int
     */
    public function getCurrentRow()
    {
        return $this->currentRow;
    }

    /**
     * Set currentCol
     *
     * @param integer $currentCol
     *
     * @return Game
     */
    public function setCurrentCol($currentCol)
    {
        $this->currentCol = $currentCol;

        return $this;
    }

    /**
     * Get currentCol
     *
     * @return int
     */
    public function getCurrentCol()
    {
        return $this->currentCol;
    }

    /**
     * Set currentDirection
     *
     * @param integer $currentDirection
     *
     * @return Game
     */
    public function setCurrentDirection($currentDirection)
    {
        $this->currentDirection = $currentDirection;

        return $this;
    }

    /**
     * Get currentDirection
     *
     * @return int
     */
    public function getCurrentDirection()
    {
        return $this->currentDirection;
    }

    /**
     * Add obstacle
     *
     * @param \AppBundle\Entity\GameObstacles $obstacle
     *
     * @return Game
     */
    public function addObstacle(\AppBundle\Entity\GameObstacles $obstacle)
    {
        $this->obstacles[] = $obstacle;

        return $this;
    }

    /**
     * Remove obstacle
     *
     * @param \AppBundle\Entity\GameObstacles $obstacle
     */
    public function removeObstacle(\AppBundle\Entity\GameObstacles $obstacle)
    {
        $this->obstacles->removeElement($obstacle);
    }

    /**
     * Get obstacles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObstacles()
    {
        return $this->obstacles;
    }
}
