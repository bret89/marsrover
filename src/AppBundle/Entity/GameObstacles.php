<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameObstacles
 *
 * @ORM\Table(name="game_obstacles")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameObstaclesRepository")
 */
class GameObstacles
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="row", type="smallint")
     */
    private $row;

    /**
     * @var int
     *
     * @ORM\Column(name="col", type="smallint")
     */
    private $col;
    
    /**
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="obstacles")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    private $game;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set row
     *
     * @param integer $row
     *
     * @return GameObstacles
     */
    public function setRow($row)
    {
        $this->row = $row;

        return $this;
    }

    /**
     * Get row
     *
     * @return int
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set col
     *
     * @param integer $col
     *
     * @return GameObstacles
     */
    public function setCol($col)
    {
        $this->col = $col;

        return $this;
    }

    /**
     * Get col
     *
     * @return int
     */
    public function getCol()
    {
        return $this->col;
    }

    /**
     * Set game
     *
     * @param \AppBundle\Entity\Game $game
     *
     * @return GameObstacles
     */
    public function setGame(\AppBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \AppBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }
    
    /**
     * Returns if given coordinate is the same as this
     * @param \AppBundle\Entity\Coordinate $coordinate
     * @return boolean
     */
    public function equalsTo(Coordinate $coordinate)
    {
        if($this->getRow() == $coordinate->getRow() && $this->getCol() == $coordinate->getCol()){
            return true;
        }
        
        return false;
    }
}
