<?php

namespace AppBundle\Entity;

class Coordinate {
    protected $row;
    
    protected $col;
    
    /**
     *
     * @var Directions
     */
    protected $direction;
    
    public function __construct($row, $col, $direction) {
        $this->row = $row;
        $this->col = $col;
        $this->direction = $direction;
    }
    
    function getRow() {
        return $this->row;
    }

    function getCol() {
        return $this->col;
    }

    /**
     * 
     * @return Directions
     */
    function getDirection() {
        return $this->direction;
    }

    function setRow($row) {
        $this->row = $row;
    }

    function setCol($col) {
        $this->col = $col;
    }

    function setDirection($direction) {
        $this->direction = $direction;
    }
}
